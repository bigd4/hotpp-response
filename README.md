# **Response to NCOMMS-24-16142**


# Timetest
Comparison of operation computation speed between Cartesian tensors and spherical harmonics tensors and LAMMPS dynamics.

# Ordertest
Comparison of accuracy using different values of $r_{max}$ and $o_{max}$.  
These tests used the code on `spinmiao` branch of [HotPP](https://gitlab.com/bigd4/hotpp), so the number of parameters may be a little different.
